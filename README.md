# cophylogeny-species-tree-quality-performance-study-data-scripts

Contains processed data and relevant scripts used in the manuscript. 
Commands to run third-party software are available in the supplementary document.
