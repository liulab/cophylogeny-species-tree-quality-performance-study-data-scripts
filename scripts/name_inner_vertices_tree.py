import pandas as pd
import numpy as np
import sys
import re
import subprocess 
import itertools
from Bio import Phylo
from io import StringIO

def get_parent(tree, child_clade):
    """
    returns the parent of a clade 
    """
    node_path = tree.get_path(child_clade)
    return node_path[-2]

def all_parents(tree):
    """
    returns a dictionary mapping all nodes to their parents
    """
    parents = {}
    for clade in tree.find_clades(order="level"):
        for child in clade:
            parents[child] = clade
    return parents

def lookup_by_names(tree):
    """
    returns a dictionary of clade names so you don't have to search tree each time.
    note: doesn't deal with unnamed, internal nodes, so make sure to give internal nodes names first.
    """
    names = {}
    for clade in tree.find_clades():
        if clade.name:
            if clade.name in names:
                raise ValueError("Duplicate key: %s" % clade.name)
            names[clade.name] = clade
    return names

def terminal_neighbor_dists(self):
    """Return a list of distances between adjacent terminals."""

    def generate_pairs(self):
        pairs = itertools.tee(self)
        pairs[1].next()
        return itertools.izip(pairs[0], pairs[1])

    return [self.distance(*i) for i in generate_pairs(self.find_clades(terminal=True))]

def _name_unnamed_nodes(tree: Phylo.Newick.Tree, tree_type: str, sep):
    count = 0
    tot_leaf=tree.count_terminals()
    for clade in tree.find_clades():
        if clade.name is None:    # internal nodes that are unlabeled
            leaves = clade.get_terminals() # name internal nodes by child leaves
            leaves_list = []
            for l in leaves:
                leaves_list.append(l.name)
            leaves_list.sort()
            if tot_leaf > len(leaves_list):  # don't name root node
                clade.name = sep.join(leaves_list)
            count += 1

if __name__ == "__main__":
    treefile=sys.argv[1]    
    outputfile=sys.argv[2]    
    tree = Phylo.read(treefile, "newick")
    
    _name_unnamed_nodes(tree, "newick","_")
    Phylo.write(tree,outputfile, "newick")
