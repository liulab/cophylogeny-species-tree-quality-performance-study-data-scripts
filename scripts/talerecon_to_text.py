import pandas as pd
import xml.etree.ElementTree as ET
import sys
import os.path
from pathlib import Path
import glob

def parse_node(node, allevents, parent=None, symbnode=None, depth=0):
    #print("\t" * depth, node.tag, node.text)
    # check if the element has the attribute 'destinationSpecies'
    if 'destinationSpecies' in node.attrib:
        destination_species = node.attrib['destinationSpecies']
        symbnode.text = symbnode.text.replace(".l","")
        df = {"symb": symbnode.text, "host": destination_species, "event": node.tag}
        #print(df)
        allevents = allevents.append(df, ignore_index=True)
    # check if the element has the attribute 'speciesLocation'
    if 'speciesLocation' in node.attrib:
        species_location = node.attrib['speciesLocation']
        symbnode.text = symbnode.text.replace(".l","")
        df = {"symb": symbnode.text, "host": species_location, "event": node.tag}
        #print(df)
        allevents = allevents.append(df, ignore_index=True)
    for child in node:
        if "name" in child.tag:
            symbnode = child
        allevents=parse_node(child, allevents, node, symbnode, depth + 1)
    return allevents

infilepath=sys.argv[1]
outevents=sys.argv[2] 

reconfiles = glob.glob(infilepath+'*')
counter=1
print(reconfiles)
for recon in reconfiles:
  #print(recon)
  # Parse the XML file
  tree = ET.parse(recon)
  root = tree.getroot()
  
  allevents=pd.DataFrame(columns = ["symb","host","event"]) # following empress order
  allevents=parse_node(root,allevents)
  allevents
  allevents.to_csv(outevents+"."+str(counter), header=False, index=False) # include leaf events
  #allevents = allevents[allevents.event != "leaf"]
  #allevents.to_csv(outevents, header=False, index=False) # exclude leaf events
  counter+=1
