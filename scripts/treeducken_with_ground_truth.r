    library(treeducken)
    library(ape)
    library(geiger)
    
    # Run Treeducken as normal
    lambda_H <- {see Treeducken parameters table}
    mu_H <- {see Treeducken parameters table}
    lambda_C <- {see Treeducken parameters table}
    lambda_S <- {see Treeducken parameters table}
    mu_S <- {see Treeducken parameters table}
    time <- {see Treeducken parameters table}
    cophy_obj <- sim_cophylo_bdp(hbr = lambda_H,
                                    hdr = mu_H,
                                    sbr = lambda_S,
                                    sdr = mu_S,
                                    cosp_rate =lambda_C,
                                    host_exp_rate = 0.0,
                                    time_to_sim = time,
                                    numbsim = 1)
                                    
    # Start modifying phylo and associations data objects to output the coevolutionary history with the event types we want
    
    ### label internal nodes ###
    label_internal_nodes <- function(tree){ #where tree is a phylo object
      tot_internal_nodes<-tree$Nnode # total number of nodes
      start_internal_nodes<-length(tree$tip.label)+1
      end_internal_nodes<-start_internal_nodes+tot_internal_nodes-1
      labels<-list()
      for (i in start_internal_nodes:end_internal_nodes){
        # nodes start incrementing from number of tips
        name<-paste(tips(tree,i),collapse = "_")
        labels <- append(labels, name)
      }
      tree$node.label <- labels
      new_tree <- write.tree(tree)
      return(new_tree)
    }
    output_unlabeled_tree<-function(tree){
      print(tree)
      new_tree <- write.tree(tree)
      return(new_tree)
    }
    #host
    write.table(output_unlabeled_tree(cophy_obj[[1]]$host_tree), file_host,
                append = FALSE, sep = " ",
                row.names = FALSE, col.names = FALSE,
                quote=FALSE)
    write.table(label_internal_nodes(cophy_obj[[1]]$host_tree), file_host_labeled,
                append = FALSE, sep = " ",
                row.names = FALSE, col.names = FALSE,
                quote=FALSE)
    #symb
    write.table(output_unlabeled_tree(cophy_obj[[1]]$symb_tree), file_symb,
                append = FALSE, sep = " ",
                row.names = FALSE, col.names = FALSE,
                quote=FALSE)
    write.table(label_internal_nodes(cophy_obj[[1]]$symb_tree), file_symb_labeled,
                append = FALSE, sep = " ",
                row.names = FALSE, col.names = FALSE,
                quote=FALSE)
    
    ### relabel event history to format: event host_node symb_node) ###
    #where tree is a phylo object
    relabel_treeducken_event_history <- function(event_history, hosttree, symbtree){
      #host trees
      tot_internal_nodes_h<-hosttree$Nnode # total number of nodes
      num_leaf_host<-length(hosttree$tip.label)
      start_internal_nodes_h<-num_leaf_host+1
      end_internal_nodes_h<-start_internal_nodes_h+tot_internal_nodes_h-1
      labels_host<-list()
      for (i in start_internal_nodes_h:end_internal_nodes_h){
        # nodes start incrementing from number of tips
        name<-paste(tips(hosttree,i),collapse = "_")
        labels_host <- c(labels_host, name)
      }
      hosttree$node.label <- labels_host
      #symb trees
      tot_internal_nodes_s<-symbtree$Nnode # total number of nodes
      num_leaf_symb<-length(symbtree$tip.label)
      start_internal_nodes_s<-num_leaf_symb+1
      end_internal_nodes_s<-start_internal_nodes_s+tot_internal_nodes_s-1
      labels_symb<-list()
      for (i in start_internal_nodes_s:end_internal_nodes_s){
        # nodes start incrementing from number of tips
        name<-paste(tips(symbtree,i),collapse = "_")
        labels_symb <- c(labels_symb, name)
      }
      symbtree$node.label <- labels_symb
      num_events<-nrow(event_history)
      events<-c()
      hosts<-c()
      symbs<-c()
      prefix_host<-"H" # H for host, S for symb
      prefix_symb<-"S"
      
      # update event names in Treeducken to the known 4 events that works with cophy software https://github.com/wadedismukes/treeducken/blob/main/src/Simulator.cpp#L682
      treeducken_events=c("SX", "HX", "SSP", "HSP", "AG", "AL", "CSP", "DISP","EXTP", "SHE", "SHS")
      known_events=c("loss", "loss", "duplication", "host_switch",
                    "duplication", "loss", "cospeciation", "cospeciation",
                    "loss", "host_switch","host_Switch")
      event_renaming=data.frame(treeducken_events, known_events)
      # mapping to known format event history
      for (i in 1:num_events){
        print(i)
        if (event_history$Event_Type[i] == "I"){
          print("Initialized")
          #skip this one, "I" stands for initialize event vector.
          next
        }
        else{
          new_event<-event_renaming$known_events[event_renaming$treeducken_events
                                                    ==event_history$Event_Type[i]]
          events <- c(events, new_event) # events
        }
        if (event_history$Host_Index[i] > num_leaf_host){ #hosts
          hosts <- c(hosts, labels_host[event_history$Host_Index[i]-num_leaf_host])
        }
        else{
          hosts <- c(hosts, paste0(prefix_host,event_history$Host_Index[i]))
        }
        if (event_history$Symbiont_Index[i] > num_leaf_symb){ #symbs
          symbs <- c(symbs, labels_symb[event_history$Symbiont_Index[i]-num_leaf_symb])
        }
        else{
          symbs <- c(symbs, paste0(prefix_symb,event_history$Symbiont_Index[i]))
        }
      } 
      new_event_history<-data.frame(events, paste(hosts, sep=" "),
                data.frame("symbs" = paste(symbs, sep=" ")))
                colnames(new_event_history) <- c("events", "hosts", "symbs")
      print(new_event_history)
      return(new_event_history)
    }
    new_event_history<-relabel_treeducken_event_history(cophy_obj[[1]]$event_history,
                cophy_obj[[1]]$host_tree, cophy_obj[[1]]$symb_tree)
    write.table(new_event_history, file_event_history,
                append = FALSE, sep = " ",
                row.names = FALSE, col.names = FALSE,
                quote=FALSE)
    ### output nexus and empress association links ###
    Which.names <- function(DF, value, file_empress_link, file_nexus_link){
      ind <- as.data.frame(which(DF==value, arr.ind=TRUE, useNames =TRUE))
      print(ind)
      num_links<-length(colnames(DF))
      links_empress<-""
      links_nexus<-""
      for (i in 1:num_links){
        symb<-colnames(association_mat)[ind$col[i]]
        host<-rownames(association_mat)[ind$row[i]]
        links_empress<-paste(links_empress,paste(symb, host ,sep=":"), sep="\n")
        links_nexus<-paste(links_nexus,paste0("'",symb,"':'",host,"',",collapse=""), sep="\n")
      }
      links_empress<-sub(".", "", links_empress) # remove first character \n
      links_nexus<-sub(".", "", links_nexus)
      cat(links_empress)
      links_nexus <- gsub(".{1}$", ";",links_nexus) # replace last character with ";"
      cat(links_nexus)
      write(links_empress, file_empress_link)
      write(links_nexus, file_nexus_link)
    }
    association_mat<-cophy_obj[[1]]$association_mat
    # where cell value is 1 means association exists
    Which.names(association_mat, 1, links_empress, links_nexus)
    cophy_obj[[1]]$host_tree$Nnode
    cophy_obj[[1]]$symb_tree$Nnode
    length(new_event_history$events)
    sum(new_event_history$events=="cospeciation")
    sum(new_event_history$events=="duplication")
    sum(new_event_history$events=="host_switch")
    num_links<-length(colnames(association_mat))
    ind <- as.data.frame(which(association_mat==1, arr.ind=TRUE, useNames =TRUE))
    all_symb=c()
    # the following only matters if the cophylogenetic software doesn't allow a symbiont to associate with multiple hosts. eMPRess and CoRe-PA don't mind.
    for (i in 1:num_links){
      symb<-colnames(association_mat)[ind$col[i]]
      if(sum(all_symb==symb) < 1){
        all_symb<-append(all_symb,symb)
      }
      else{
        print("symb lineage on multiple hosts.")
        break
      }
    }
