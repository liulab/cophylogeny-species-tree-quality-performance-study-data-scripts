import pandas as pd
import numpy as np
from ete3 import Tree
import sys
import re
import subprocess 
from random import sample

pd.set_option('display.max_columns', None)
def calc_precision_innervertices(shared_items, ori_only,sim_only):

    try:
        prec = (shared_items) / (shared_items + ori_only)
    except ZeroDivisionError:
        prec = "Undefined"
    return prec

def set_comparison_dictionaries(dict1, dict2):
    """
    Input: 2 dictionaries with the same columns
    Output: num of same rows, num of unique rows in dict 1, number of unique rows in dict 2
    """
    df = pd.merge(dict1, dict2, on=["Event", "Host", "Symb"], how='left',indicator='Exist')

    #df = pd.merge(dict1, dict2, on=["Event", "Host", "Symb", "Switched_host"], how='left',indicator='Exist')
    shared_items = df[df.Exist == 'both'].shape[0] 
    dict1_only = df[df.Exist == 'left_only'].shape[0] 
    dict2_only = dict2.shape[0] - shared_items
    """
    print("df[df.Exist == 'both']")
    print(df[df.Exist == 'both'])
    print("dict1_only")
    print(dict1_only)
    print("dict2_only")
    print(dict2_only)
    """
    return shared_items, dict1_only,dict2_only

def cophy_empress_regex(recon):
    """
    Parse input string into two dictionaries. 
    Output: dict_recon contains rows of events, dict_numevents contains number of each event type 
    """
    recon = recon.replace("p0","P0")
    recon = recon.replace("h0","H0")
    temp=recon.split()

    dict_recon = pd.DataFrame(index=range(0, len(temp)-1) ,columns=["Event", "Host", "Symb", "Switched_host"])
    num_events=len(temp)

    # append to dictionary all events
    for i in range(0, num_events):
        line = temp[i].split(",")
        # append to dictionary
        print(frozenset(line[0].split('_')))
        dict_recon.at[i, "Symb"] = frozenset(line[0].split('_')) #line[0]
        dict_recon.at[i, "Host"] = frozenset(line[1].split('_')) #line[1]
        event = line[2]
        if event == "Transfer": # rename Transfer to Host_switch
            dict_recon.loc[i, "Event"] = "host_switch"
        else:
            dict_recon.loc[i, "Event"] = line[2].lower()
    # line[3] and line[4] are values, likely branch lengths when these occured on both trees?

    dict_recon = dict_recon[dict_recon['Event'] != "Contemporaneous"] # their way of saying Tip, which we remove bc it's not an event that occurred on a branch

    dict_recon = dict_recon.astype(object)
    print(dict_recon)
    return dict_recon

def cophy_jane_regex(recon):
    """
    Parse input string into two dictionaries. 
    Output: dict_recon contains rows of events, dict_numevents contains number of each event type 
    """
    temp=recon.split("--------------------------")
    # create dictionary for reconciliations
    dict_recon = pd.DataFrame(np.nan, index=range(0, len(temp)-1) ,columns=["Event", "Host", "Symb", "Switched_host"])
    dict_numevent = pd.DataFrame(np.nan, index=[0], columns=["Cospeciation","Duplication", "Host_switch", "Loss", "Failure_to_diverge"])
    num_events=len(temp)
    
    # append to dictionary all events
    for i in range(0, num_events):
         chunk = temp[i]
         # info to store:
         symb = re.search('(?<=Parasite Node: )(.*)', chunk).group(1)
         event = re.search('(?<=Association type: )(.*)', chunk).group(1)
         host = re.search('(?<=Host: )(.*)',chunk).group(1)
     
         if event == "Host Switch":
             switched_host = re.search('(?<=Switch Target: )(.*)',chunk	).group(1)
             dict_recon.loc[i, "Switched_host"] = switched_host
    
         # append to dictionary
         dict_recon.loc[i, "Event"] = event
         dict_recon.loc[i, "Symb"] = symb
         dict_recon.loc[i, "Host"] = host
    # store information about number of events
    chunk=temp[-1]
    dict_numevent.loc[0, "Cospeciation"] = int(re.search('(?<=Cospeciation: )(.*)', chunk).group(1))    
    dict_numevent.loc[0, "Duplication"] = int(re.search('(?<=Duplication: )(.*)', chunk).group(1))
    dict_numevent.loc[0, "Host_switch"] = int(re.search('(?<=Host Switch: )(.*)', chunk).group(1))
    dict_numevent.loc[0, "Loss"] = int(re.search('(?<=Loss: )(.*)', chunk).group(1))
    dict_numevent.loc[0, "Failure_to_diverge"] = int(re.search('(?<=Failure to Diverge: )(.*)', chunk).group(1))
    #print(dict_numevent)
    #print(dict_recon.dtypes)
    dict_recon = dict_recon[dict_recon['Event'] != "Tip"]
    dict_recon = dict_recon.astype(object)
    print(dict_recon)
    return dict_recon, dict_numevent

def cophy_parafit_regex(recon):
    """
    Parse input string into two dictionaries. 
    Output: dict_recon contains rows of events, dict_numevents contains number of each event type 
    """
    threshold = 0.05 #arbitrary parafit F1 significance based on original paper
    r = []
    string = recon.strip().split('\n')
    for i in string:
        if i.startswith('[')  :
            r.append(re.sub(' +', ' ', i))
    df = pd.DataFrame([x.split() for x in r])
    print(df)
    row_count = 0
    host = []
    symb = []
    for a, b in df.iterrows():
        if float(re.sub('\'', '', b[4])) <= threshold:
            row_count += 1
            host.append(b[1])
            symb.append(b[2])
    # events    
    dict_recon = pd.DataFrame(np.nan, index=range(0, row_count) ,columns=["Event", "Host", "Symb", "Switched_host"])
    for i in range(row_count):
        dict_recon.loc[i, "Host"] = host[i]
        dict_recon.loc[i, "Symb"] = symb[i]
    # number of events
    dict_numevent = pd.DataFrame(np.nan, index=[0], columns=["Cospeciation","Duplication", "Host_switch", "Loss", "Failure_to_diverge"])
    dict_numevent.loc[0, "Cospeciation"] = row_count 
    return dict_recon, dict_numevent
def cophy_corepa_regex(recon):
    """
    Parse input string into two dictionaries.
    Output: dict_recon contains rows of events, dict_numevents contains number of each event type
    """
    recon = recon.replace(":1e-06","")
    temp=recon.split("RECONSTRUCTION")
    temp1 = temp[1].split("EVENTASSOCIATIONSPERHOST")
    temp = temp1[1].split("EVENTASSOCIATIONSPERPARASITE")
    links = temp1[0].split("\n")
    h = temp[0].split("\n")
    s = temp[1].split("\n")

    # hosts and symbs that participate in events are stored in dicts. Corepa operations 'COSPECIATION', 'DUPLICATION', 'SORTING' and 'HOSTSWITCH'
    num_hosts = len([x for x in h if x])
    num_symbs = len([x for x in s if x])
    dict_h = pd.DataFrame(np.nan, index=range(1, num_hosts) ,columns=["Cospeciation","Duplication", "Sorting", "Host_switch"])
    dict_s = pd.DataFrame(np.nan, index=range(1, num_symbs) ,columns=["Cospeciation","Duplication", "Sorting", "Host_switch"])
    for i in range(1, len(h)):
        assoc = h[i].split("\t")
        if len(assoc) > 1:
            dict_h.loc[i, "Cospeciation"] = assoc[1]
            dict_h.loc[i, "Duplication"] = assoc[2]
            dict_h.loc[i, "Sorting"] = assoc[3]
            dict_h.loc[i, "Host_switch"] = assoc[4]
            dict_h.rename(index={i:assoc[0]},inplace=True)

    for i in range(1, len(s)):
        assoc = s[i].split("\t")
        if len(assoc) > 1:
            dict_s.loc[i, "Cospeciation"] = assoc[1]
            dict_s.loc[i, "Duplication"] = assoc[2]
            dict_s.loc[i, "Sorting"] = assoc[3]
            dict_s.loc[i, "Host_switch"] = assoc[4]
            dict_s.rename(index={i:assoc[0]},inplace=True)
    dict_s = dict_s.astype(int)
    dict_h = dict_h.astype(int)
    # event associations between hosts and symbs. Nothing gets stored in Switched_host since corepa doesn't say what host switched to
    num_links = len([x for x in s if x])
    dict_recon = pd.DataFrame(index=range(1, num_links) ,columns=["Event", "Host", "Symb"])
    all_events=["Cospeciation","Duplication", "Sorting", "Host_switch"]

    for i in range(1, len(links)):  # append to dictionary all events
        event = links[i].split("\t")
        if len(event) > 1:
            host = event[1]
            symb = event[0]
            # append to dictionary
            for evnt in all_events:
                if dict_h.loc[host,evnt] > 0 and dict_s.loc[symb,evnt] > 0:
                    dict_recon.loc[i, "Event"] = evnt.lower()
                    dict_h.loc[host,evnt] = dict_h.loc[host,evnt] - 1
                    dict_s.loc[symb,evnt] = dict_s.loc[symb,evnt] -1
            dict_recon.at[i, "Host"] = frozenset(host.split('_'))
            dict_recon.at[i, "Symb"] = frozenset(symb.split('_'))
    dict_recon = dict_recon[dict_recon['Event'].notnull()] # their way of saying Tip
    dict_recon = dict_recon.astype(object)
    print(dict_recon)
    return dict_recon

def isNaN(num):
    return num != num
def call_precision_func(ori_recon,sim_recon,outfile):
    # count up same and differences, calculate precision
    shared_items, ori_only,sim_only = set_comparison_dictionaries(ori_recon, sim_recon)    # number of shared / not shared inner vertices events
    # num_same, num_diff = compare_cells_dictionaries(ori_numevent, sim_numevent)    # number of shared / not shared summary events (# cospeciation, # loss, etc)
    prec = calc_precision_innervertices(shared_items, ori_only,sim_only)
    print("All events")
    print("Shared items: "+str(shared_items))
    print("Original only: "+str(ori_only))
    print("Simulated only: "+str(sim_only))
    print("Precision: "+str(prec))
    
    # append to outfile
    with open(outfile,'a+') as out:
        out.write(str(prec)+"\n")

def call_precision_func_specify_event(ori_recon,sim_recon,outfile, specify_event):
    # filter for only the requested event
    ori_recon = ori_recon[ori_recon['Event'] == specify_event]
    sim_recon = sim_recon[sim_recon['Event'] == specify_event]
    outfile = outfile+"_"+specify_event # give it its own name

    # count up same and differences, calculate precision
    shared_items, ori_only,sim_only = set_comparison_dictionaries(ori_recon, sim_recon)    # number of shared / not shared inner vertices events
    # num_same, num_diff = compare_cells_dictionaries(ori_numevent, sim_numevent)    # number of shared / not shared summary events (# cospeciation, # loss, etc)

    num_same = 0
    num_diff = 0
    prec = calc_precision_innervertices(shared_items, ori_only,sim_only)
    print()
    print(specify_event)
    print("Shared items: "+str(shared_items))
    print("Original only: "+str(ori_only))
    print("Simulated only: "+str(sim_only))
    print("Precision: "+str(prec))
    
    # append to outfile
    with open(outfile,'a+') as out:
        out.write(str(prec)+"\n")

if __name__ == "__main__":
    ori_reconciliation=sys.argv[1]    
    sim_reconciliation=sys.argv[2]    
    recon_app=sys.argv[3].lower() 
    outfile=sys.argv[4]

    # open reconciliation files
    with open(ori_reconciliation,"r") as infile:
        original_recon = infile.read()
    with open(sim_reconciliation,"r") as infile:
        simulated_recon = infile.read()

    # sort by software regex to use to parse the data
    if recon_app == "jane":
        print("Jane4")
        ori_recon, ori_numevent = cophy_jane_regex(original_recon.split("==================================")[1])
        sim_recon, sim_numevent = cophy_jane_regex(simulated_recon.split("==================================")[1])
    elif recon_app == "empress":
        print("eMPRess")
        print("original cophylogeny estimation")
        ori_recon = cophy_empress_regex(original_recon)
        print("simulated cophylogeny estimation")
        sim_recon = cophy_empress_regex(simulated_recon)
    elif recon_app == "corepa":
        print("corepa")
        # original reconciliation is done with empress
        ori_recon = cophy_empress_regex(original_recon)
        #original_recon_split = original_recon.split("\nCOSTS\n")
        simulated_recon_split = simulated_recon.split("\nCOSTS\n")
        #print(original_recon_split)
        
    elif recon_app == "parafit":
        print("Parafit (R)")
        ori_recon, ori_numevent = cophy_parafit_regex(original_recon.split("      Host Parasite       F1.stat  p.F1       F2.stat  p.F2")[1])
        sim_recon, sim_numevent = cophy_parafit_regex(simulated_recon.split("      Host Parasite       F1.stat  p.F1       F2.stat  p.F2")[1])
    elif recon_app == "axparafit":
        print("AxParafit")
    else:
       raise ValueError("Your reconciliation software "+recon_app+" was not recognized.")

    all_events=["Cospeciation", "Duplication", "Host_switch", "Loss"]
    if recon_app == "corepa": # special as it can report multiple reconstructions instead of picking just one
        num_comps = 10 # ceiling of comparisons. 
        comps = 1
        for j in simulated_recon_split:      # calculate for that subset
            if comps <= num_comps:
                sim_recon=cophy_corepa_regex(j)
                call_precision_func(ori_recon,sim_recon,outfile)
                comps += 1
                #for specify_event in all_events:
                #    print(specify_event)
                #    call_precision_func_specify_event(ori_recon,sim_recon,outfile, specify_event)
        with open(outfile+"_nRF_repeats","a+") as out:
            if comps > num_comps:
                out.write(str(num_comps)+"\n")
            else:
                out.write(str(comps)+"\n")

    else:
        call_precision_func(ori_recon,sim_recon,outfile)
        for specify_event in all_events:
            call_precision_func_specify_event(ori_recon,sim_recon,outfile, specify_event)
